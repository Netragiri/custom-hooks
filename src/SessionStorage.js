import React from 'react'
import useSessionStorage from './useSessionStorage'

function LocalStorage() {
    const {additem,changehandler,item}=useSessionStorage('name','netra')
  return (
    <div>
       <form onSubmit={(e) =>{additem(e)}}>
           <input type='text' value={item} onChange={changehandler}></input>
           <input type='submit'></input>
       </form>
    </div>
  )
}

export default LocalStorage

