import React,{useState} from 'react'
import useCopy from './useCopy'

function CopytoClipboard() {
    
  const[input,setInput]=useState("")

  const handlechange=(e)=>{
    setInput(e.target.value)
}
    const{copyhandler,result}=useCopy("")

    const finalresult = () => {
        copyhandler(input)
    }
  return (
    <div>
      <input type='text' onChange={handlechange} value={input} ></input>
      <button onClick={finalresult}>Copy text </button>
    </div>
  )
}

export default CopytoClipboard
