import { useState } from "react"


function useForm() {
    const [name,setName]=useState('')
    const [lastname,setLastName]=useState('')
    const handlechange=(e,name)=>{
        if(name == 'firstname'){

            setName(e.target.value)
        }else{

            setLastName(e.target.value)
        }
    }
    
    const submithandler=(e)=>{
        e.preventDefault()
        if(name.length==0 || lastname.length==0){
            alert("name & lastname can not be empty")
        }
        setName("");
        setLastName("");
    }
   return ({submithandler,handlechange,name,lastname})
}

export default useForm
