import React from 'react'
import useLocalStorage from './useLocalStorage'

function LocalStorage() {
    const {additem,changehandler,item}=useLocalStorage('name','netra')
  return (
    <div>
       <form onSubmit={(e) =>{additem(e)}}>
           <input type='text' value={item} onChange={changehandler}></input>
           <input type='submit'></input>
       </form>
    </div>
  )
}

export default LocalStorage
