import { useState ,useEffect} from "react";

const getSize=()=>{
    return{
        height:window.innerHeight,
        width:window.innerWidth
        
    }

}

function useWindow() {
  const [windowssize,setWindowsSize]=useState(getSize())


const resizehandler=()=>{

    setWindowsSize(getSize())
}

  useEffect(()=>{
      window.addEventListener("resize",resizehandler);


      return()=>{
          window.removeEventListener('resize',resizehandler)
      }
  },[])
  return windowssize
}

export default useWindow
