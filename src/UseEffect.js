import React, { useEffect, useState } from 'react'

function UseEffect() {

    const[count,setCount]=useState(98)

   
 useEffect(()=>{
     let Timer=setTimeout(()=>{
        setCount(count-777)
     },2000)


 return()=>
     clearTimeout(Timer)
    },[]);
  return (
    <div>
      
     <p>count:{count}</p>
    </div>
  )
}

export default UseEffect
