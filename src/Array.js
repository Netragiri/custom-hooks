import React from 'react'
import useArray from './useArray'

function Array() {
    const{array,push,empty,pop}=useArray([45,23,56,87,55,99])
  return (
    <div>
      <h3>{array.join(',')}</h3>

      <button onClick={()=>push(34)}>Add</button><br />

      <button onClick={()=>empty()}>Remove</button><br />

      <button onClick={()=>pop(2)}>remove value at 2nd index</button><br />


    </div>
  )
}

export default Array
