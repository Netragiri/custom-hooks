import React from 'react'
import useWindow from './useWindow'

function Window() {
    const windowssize=useWindow();
  return (
    <div>
      height of window: {windowssize.height} and width :{windowssize.width}
    </div>
  )
}

export default Window
