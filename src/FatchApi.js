import React from 'react'
import useFatch from './useFatch'

function FatchApi() {
    const{data,error}=useFatch('https://jsonplaceholder.typicode.com/posts')
  
    console.log(data)
  return (
    <div>
         {data.map(item=>
        <div key={item.id}>
            <p>{item.id},{item.title}</p>

            <p>{error}</p>
        </div>
        )}
        
    </div>
  )
}

export default FatchApi
