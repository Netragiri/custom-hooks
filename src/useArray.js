import React, { useState } from 'react'

function useArray(defaultvalue) {
  const[array,setArray]=useState(defaultvalue)

  function push(value){
    setArray(a=>[...a,value])
  }

  function empty(){
      setArray([])
  }

 function pop(index){
     setArray(a => [...a.slice(0, index), ...a.slice(index + 1, a.length)])
 }
return {array,push,empty,pop}
}
export default useArray

