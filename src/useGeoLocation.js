import React, { useEffect, useState } from 'react'

function useGeoLocation() {
  const[location,setLocation]=useState({
      cordinates:{latitude:'',longitude:''},
      status:false
  })

  const success=(location)=>{
    setLocation({
      status:true,
      cordinates:{
        latitude:location.coords.latitude,
        longitude:location.coords.longitude,
      }
    })
  }
  const error=(error)=>{
    setLocation({
      status:true,
      error:{
        message:error.message
      }
    })
  }

  useEffect(()=>{
    if (!("geolocation" in navigator)) {
      error({
        message:'location not supported on this browser'
      })
    }

    navigator.geolocation.getCurrentPosition(success,error)
  },[])
  return {location}
}

export default useGeoLocation
