import { useState } from "react"


function useLocalStorage() {
    const[item,setItem]=useState("")

    const additem=(e)=>{
      
        e.preventDefault();

        localStorage.setItem("name",item);
        
        console.log('new name is save ==>',item);

      let name =  localStorage.getItem("name");
      console.log('get item is ==>',name);
    }
    const changehandler=(e)=>{
        setItem(e.target.value)
    }
    console.log(item);
  return (
   {additem,item,changehandler}
  )
}

export default useLocalStorage
