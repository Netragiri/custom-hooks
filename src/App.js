import React from 'react'
import UseState from './UseState'
import UseEffect from './UseEffect'
import Array from './Array'
import Window from './Window'
import Form from './Form'
import CopyToClipboard from './CopytoClipboard'
import FatchApi from './FatchApi'
import LocalStorage from './LocalStorage'
import SessionStorage from'./SessionStorage'
import Geolocation from './Geolocation'

function App() {
  return (
    <div>
      {/* <UseState/>
      <UseEffect/> */}
      {/* <Array /> */}
      {/* <Window /> */}
      {/* <Form /> */}
      {/* <CopyToClipboardComponent /> */}
      {/* <FatchApi /> */}
      {/* <LocalStorage /> */}
      {/* <SessionStorage /> */}
      {/* <CopyToClipboard /> */}
      <Geolocation />
      

    </div>
  )
}

export default App
