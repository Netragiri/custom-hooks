import React from 'react'
import useGeoLocation from './useGeoLocation'

function Geolocation() {
    const {location}=useGeoLocation()
  return (
    <div>
      {location.status?JSON.stringify(location):'location not available'}
    </div>
  )
}

export default Geolocation
