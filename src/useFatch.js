import React, { useEffect, useState } from 'react'
import axios from 'axios'

function useFatch(url) {
    const[data,setData]=useState([])
    const[error,setError]=useState(null)

 useEffect(()=>{
    axios.get(url).then(response=>
        {setData(response.data)})
    .catch(reject=>{setError(reject)})

 },[url])

 return {data,error}
}

export default useFatch
