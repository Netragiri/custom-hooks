import React from 'react'
import useForm from './useForm'

function Form() {
    const {handlechange,name,lastname,submithandler}=useForm('')
  return (
    <div>
        <form onSubmit={submithandler}>
            Name:
            <input value={name} onChange={(e)=>{handlechange(e,'firstname')}} type="text" name='firstname' placeholder='Enter name' ></input><br />

            Lastname:
            <input value={lastname} onChange={(e)=>{handlechange(e,'lastname')}} name='lastname' type="text" placeholder='Enter surname' ></input>

            <input type='submit'></input>
        </form>
    </div>
  )
}

export default Form
